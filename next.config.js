module.exports = {
  reactStrictMode: true,
  i18n: {
    locales: ["en-US", "bn-BD"],
    defaultLocale: "en-US",
  },
};
