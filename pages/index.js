import axios from "axios";
import Link from "next/link";
import styles from "../styles/Home.module.css";
import HeroSection from "./components/HeroSection";
import { API_URL } from "../constant";

export default function Home({ landingPage }) {
  console.log(landingPage)
  const { Hero, services } = landingPage;
  return (
    <div className={styles.container}>
      <HeroSection
        heading={Hero.Heading}
        subHeading={Hero.SubHeading}
        callToAction={Hero.CallToAction}
      />
      <div className={styles.Services}>
        {services.map((service) => (
          <div className={styles.ServiceItem} key={service.id}>
            <h1>{service.title}</h1>
            <span>{service.description}</span>
            <Link href={`/services/${service.id}`} passHref>
              <a>Know More</a>
            </Link>
          </div>
        ))}
      </div>
    </div>
  );
}

export const getServerSideProps = async (context) => {
  console.log(context.query);
  const { locale } = context.query;

  try {
    console.log(`${API_URL}/landing-page${locale ? `?_locale=${locale}` : ""}`)
    const res = await axios.get(
      `${API_URL}/landing-page${locale ? `?_locale=${locale}` : ""}`
    );
    return { props: { landingPage: res.data } };
  } catch (error) {
    return { props: { error } };
  }
};
