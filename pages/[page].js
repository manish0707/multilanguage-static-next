import axios from "axios";
import { COMPONENT_MAP } from "../constant";

export default function Page({ pageData }) {
  const { sections, pageTitle } = pageData;
  return (
    <div>
      <h1 style={{ textAlign: "center" }}>{pageTitle}</h1>
      {sections?.map(({ componentName, ...rest }) => {
        const DynamicComponent = COMPONENT_MAP[componentName.componentName];
        return <DynamicComponent key={rest.id} {...rest} />;
      })}
    </div>
  );
}

export const getStaticPaths = async (context) => {
  // console.log(context, "path")
  try {
    const res = await axios.get(`http://localhost:1337/pages`);
    // console.log(res.data)
    const locale = context.locales[1];

    const paths = res.data.map((page) => ({ params: { page: page.slug } }));

    const resLocale = await axios.get(
      `http://localhost:1337/pages?_locale=${context.locales[1]}`
    );
    const localePaths = resLocale.data.map((page) => ({
      params: { page: page.slug },
      locale,
    }));

    return { paths: [...paths, ...localePaths], fallback: false };
  } catch (error) {
    return { paths: [], fallback: false };
  }
};

export const getStaticProps = async (context) => {
  const { locale, defaultLocale } = context;
  const slug = context.params.page;
  try {
    const res = await axios.get(
      `http://localhost:1337/pages?slug=${slug}${
        locale === defaultLocale ? "" : `&_locale=${locale}`
      }`
    );
    return { props: { pageData: res.data[0] }, revalidate: 60 };
  } catch (e) {
    return { props: {}, revalidate: 60 };
  }
};
