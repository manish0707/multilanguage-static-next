import React from "react";

export default function CardList({ cardItems }) {
  // console.log({ cardItems });
  return (
    <div>
      <h2>Card List</h2>
    <div style={{display: 'flex'}}>
    {cardItems.map(({ id, title, description, action }) => (
        <div key={id} style={{ width: "20em", border: '1px solid', margin: '10px', padding: '20px', borderRadius: '20px'}}>
          <h1>{title}</h1>
          <p>{description}</p>
          <button style={{ padding: "10px 30px" }}>{action}</button>
        </div>
      ))}
    </div>
    </div>
  );
}
