import { useRouter } from "next/router";
import React from "react";
import styles from "./HeroSection.module.css";

export default function HeroSection({ Heading, SubHeading, CallToAction }) {
  const router = useRouter();

  const handleSelectLanguage = (e) => {
    const selection = e.target.value;

    if (selection === "English") {
      return router.push("/");
    }

    return router.push(`/?locale=bn-BD`);
  };
  return (
    <div className={styles.Hero}>
      <select style={{fontSize: '16px', padding: '10px'}} onChange={handleSelectLanguage} placeholder="Change Language">
        <option>English</option>
        <option>Bangladesh</option>
      </select>
      <h1>{Heading}</h1>
      <span>{SubHeading}</span>
      <button className={styles.btn}>{CallToAction}</button>
    </div>
  );
}
