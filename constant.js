export const  API_URL = process.env.API_URL;

import CardList from "./pages/components/CardList";
import HeroSection from "./pages/components/HeroSection";

export const COMPONENT_MAP = {
    'Hero': HeroSection,
    'CardList' : CardList,
}